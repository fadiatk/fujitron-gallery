

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity
} from 'react-native';
import StackNav from './src/Navigation/StackNav';
import Store from './src/Context/Store'




const App: () => React$Node = () => {
  return (
    <Store>
      <NavigationContainer>
        <StackNav />
      </NavigationContainer>
    </Store>
  );
};




export default App;
