import React, { useEffect, useRef, useState } from 'react';

import AsyncStorage from '@react-native-async-storage/async-storage';

// export const UserDataContext = React.createContext();
export const GalleryContext = React.createContext();

const Store = ({ children }) => {


    const [media, setMedia] = useState([]);


    const onAdd = (obj) => {
        // console.log('obj in store onAdd', obj);

        var obj = {
            mediaName: obj.modificationDate,
            mediaURL: obj.path,
            mediaType: obj.mime,
        }
        if (media) {
            setMedia([...media, obj]);
            // console.log('media', media)
        }else{
            setMedia( obj);
            // console.log('media1', media)

        }


    }


    

    const onRemove = (obj) => {

        var obj = {
            mediaName: obj.mediaName,
            mediaURL: obj.mediaURL,
            mediaType: obj.mediaType,
        }
        var tempArr = media;

        console.log("temp Array", obj);

         setMedia(media.filter((x) => x.mediaName !== obj.mediaName));
          console.log('media after deleting', media);

    }



    return (
        <GalleryContext.Provider value={{
            media: [media, setMedia],
            onAdd: { onAdd },
            onRemove: { onRemove }
        }}>
            {children}

        </GalleryContext.Provider>

    )
}

export default Store;