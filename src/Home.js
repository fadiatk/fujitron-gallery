
import React , {useContext, useEffect} from 'react';
import { View, Text, TouchableOpacity, StyleSheet, SafeAreaView, Image, Dimensions } from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-action-sheet';
import { GalleryContext } from './Context/Store';
import AsyncStorage from '@react-native-async-storage/async-storage';



let { width: screenWidth, height: screenHeight } = Dimensions.get('window')

function Home({navigation}) {

  const { media, onAdd } = useContext(GalleryContext);
  const [uMedia, setUMedia] = media;
  var uOnAdd = onAdd;

  useEffect(() => {

    storeMedia();

}, [uMedia]);


const storeMedia = async () => {
    console.log('in storeMedia', uMedia);
    try {
        await AsyncStorage.setItem('media', JSON.stringify(uMedia));
        const checkasync = await AsyncStorage.getItem('media');;
        console.log('checkasync1', checkasync);
    } catch (error) { }
}


  var options = [
    'Photo',
    'Video',
    'Cancel'
  ];

  const handleUploadMedia = (() => {

    

    ActionSheet.showActionSheetWithOptions({
      options: options,
      tintColor: 'blue',
      cancelButtonIndex: 3
    },
      (buttonIndex) => {

        switch (buttonIndex) {
          case 0:
            ImagePicker.openCamera({
              mediaType: 'photo',
            }).then(image => {
              uOnAdd.onAdd(image);
              navigation.navigate('galleryView');

              // console.log('media in home', uMedia)
            });

            break;

          case 1:
            ImagePicker.openCamera({
              mediaType: 'video'
            }).then(image => {
              uOnAdd.onAdd(image);
              navigation.navigate('galleryView');


            });

            break;
        }
      })


  })

  
  return (
    <SafeAreaView style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: "#ffffff" }}>
         <Image source={require('./assets/images/picture.png')} 
         style={{  height: 220, width: screenWidth * 0.7 , }} 
         
         resizeMode={'contain'}/>


      <View style={{ width: '80%', }}>
        <TouchableOpacity
          style={[structure.buttonOutline]}
          onPress={() => { handleUploadMedia()}}
        >
          <Text style={[structure.buttonText, {color:'#92b5fa'}]}>Click!</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[structure.button]}
          onPress={() => { navigation.navigate('galleryView')}}
        >
          <Text style={[structure.buttonText,]}>View Gallery</Text>
        </TouchableOpacity>


      </View>
    </SafeAreaView>

  )

}

const structure = StyleSheet.create({
  button: {
    width: '80%',
    height: 49,
    borderColor: '#92b5fa',
    borderStyle: 'solid',
    borderWidth: 1,
    backgroundColor: '#92b5fa',
    alignItems: "center",
    borderRadius: 50,
    justifyContent: 'center',
    alignSelf: 'center',
    padding: 10,
    marginBottom: 30,
  },
  buttonOutline: {
    width: '80%',
    height: 49,
    borderColor: '#92b5fa',
    borderStyle: 'solid',
    borderWidth: 1.5,
    backgroundColor: '#ffffff',
    alignItems: "center",
    borderRadius: 50,
    justifyContent: 'center',
    alignSelf: 'center',
    padding: 10,
    marginBottom: 30,
  },
  buttonText: {
    textAlign: 'center',
    fontSize: 17,
    color: "#ffffff",
    letterSpacing: 1,
    fontFamily: 'NunitoSans-Bold',
  },

})

export default Home;