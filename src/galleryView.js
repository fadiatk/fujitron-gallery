import React, { useContext, useEffect, useState } from 'react';
import { Alert, View, Text, TouchableOpacity, StyleSheet, SafeAreaView, FlatList, Image, Dimensions } from 'react-native';
import { GalleryContext } from './Context/Store';
import AsyncStorage from '@react-native-async-storage/async-storage';
import FileViewer from 'react-native-file-viewer';

let { width: screenWidth, height: screenHeight } = Dimensions.get('window')

function galleryView() {



    const { media, onAdd, onRemove } = useContext(GalleryContext);
    const [uMedia, setUMedia] = media;
    const [flatMedia, setFlatMedia] =useState([]);


    useEffect(() => {

        if (uMedia.length) {

            const tempData = JSON.parse(JSON.stringify(uMedia));
            setFlatMedia(tempData);

        } 

        
    
    }, []);
    useEffect(() => {

        if (uMedia.length>0) {

            const tempData = JSON.parse(JSON.stringify(uMedia));
            setFlatMedia(tempData);

        } 
        else{
            setFlatMedia([]);
        }
        
    
    }, [uMedia]);
    

    var uOnAdd = onAdd;
    var uOnRemove = onRemove;


    function formatData(data, numColumns) {
        if (data) {
            const numOfFullRows = Math.floor(data.length / numColumns);
            let numOfElements = data.length - (numOfFullRows * numColumns);
            while (numOfElements !== numColumns && numOfElements !== 0) {
                data.push({ key: `blank-${numOfElements}`, empty: true });
                numOfElements = numOfElements + 1;
            }
        }
        return data;
    }


    function listEmptyComponent() {
        return (
            <View style={structure.emptyView}>
                <Text>There are no images or videos</Text>
            </View>
        )

    }

    const handleView = async (item) => {

        const uri = item.mediaURL;

        FileViewer.open(uri, { showOpenWithDialog: true, showAppsSuggestions: true })
            .then(() => {
                //Can do anything you want after opening the file successfully
                console.log('Success');
                // res.flush();
            })
            .catch(err => {
                console.log(err);
            });
    };


function handleDeleteImage(item){
    Alert.alert(
        "Gallery App",
        "Are you sure you want to delete this?",
        [
            {
                text: "Cancel",
                style: "cancel"
            },
            { text: "Yes", onPress: () => handleDelete(item) }
        ]
    );
}
    function handleDelete(item) {
        console.log(' smceecber', item)
        uOnRemove.onRemove(item)

    }

    return (
        <View style={{ flex: 1, padding: 10, backgroundColor: '#ffffff', }}>

            <View style={[structure.homeRowView]}>

                <FlatList
                    data={formatData(flatMedia, 2)}
                    ListEmptyComponent={listEmptyComponent}
                    showsVerticalScrollIndicator={false}
                    columnWrapperStyle={{  justifyContent: 'space-around', }}
                    renderItem={({ item }) => {
                        if (item.empty === true) {
                            return (
                                <View style={{ flex: 1, backgroundColor: '#ffffff', marginBottom: 20}}>
                                    <View style={[structure.homeRowItem, structure.itemInvisible]} />
                                </View>
                            )
                        }
                        return (
                            <View style={{ backgroundColor: '#ffffff', marginBottom: 20, marginTop: 5,alignSelf: 'center', flex:1,  }}>
                                <View style={[structure.homeRowItem]}>
                                    <TouchableOpacity onPress={() => { handleView(item) }}>
                                        <View style={structure.homeImageView}>
                                            <Image
                                                source={{ uri: item.mediaURL }}
                                                style={structure.homeIcon1}
                                                resizeMode={'cover'}
                                            />

                                        </View>
                                        <View>
                                            {
                                                (item.mediaType).slice(0, 5) === 'video' &&
                                                <Image source={require('./assets/images/play-button.png')} style={{ marginLeft: 2, position: 'absolute', zIndex: 2, right: 30, left: 55, bottom: 55, height: 30, width: 30 }} />
                                            }
                                        </View>

                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => { handleDeleteImage(item) }} style={structure.buttonOutline}>
                                        <Text style={structure.buttonText}>Delete</Text>
                                        <Image source={require('./assets/images/delete.png')} style={{   marginLeft:10 ,height: 12, width: 12  ,}} />
                                    </TouchableOpacity>

                                </View>
                            </View>

                        );
                    }
                    }
                    numColumns={2}
                />
            </View>


            {/* <FlatList
                    data={uMedia}
                    ListEmptyComponent={listEmptyComponent}
                    showsVerticalScrollIndicator={false}
                    columnWrapperStyle={{  justifyContent: 'space-around', alignItems: 'flex-start'}}
                    keyExtractor={(item,index) =>index.toString()}
                    renderItem={({ item }) => {
                      
                        return (
                            <View style={{ backgroundColor: '#ffffff', marginBottom: 20, marginTop: 5,alignItems: 'flex-start', flex:1  }}>
                                    <TouchableOpacity onPress={() => { handleView(item) }} >
                                            <Image
                                                source={{ uri: item.mediaURL }}
                                                style={structure.homeIcon}
                                                resizeMode={'cover'}
                                            />

                                    
                                        <View>
                                            {
                                                item && (item.mediaType).slice(0, 5) === 'video' &&
                                                <Image source={require('./assets/images/play-button.png')} style={{ marginLeft: 2, position: 'absolute', zIndex: 2, right: 30, left: 55, bottom: 55, height: 30, width: 30 }} />
                                            }
                                        </View>

                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => {  handleDeleteImage(item) }} style={structure.buttonOutline}>
                                        <Text style={structure.buttonText}>Delete</Text>
                                        <Image source={require('./assets/images/delete.png')} style={{   marginLeft:10 ,height: 12, width: 12  ,}} />
                                    </TouchableOpacity>

                            </View>

                        );
                    }
                    }
                    numColumns={2}
                />   */}

        </View>
    )

}


const structure = StyleSheet.create({
    homeRowView: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'space-around',
        marginBottom: 0,
        alignItems: 'flex-start'
    },
    homeImageView: {
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        // borderRadius:20,
        // borderWidth:1,

    },
    homeIcon1: {
        width: 150,
        height: 150,
        alignSelf: 'center',
        //resizeMode:''

    },
    homeIcon: {
        width: screenWidth/2,
        height: screenWidth/2,
        alignSelf: 'center',
        //resizeMode:''

    },
    homeRowItem: {
        flexDirection: 'column',
        //justifyContent: 'center',
        marginBottom: 0,
        alignItems: 'center'
    },
    itemInvisible: {
        backgroundColor: 'transparent',
    },
    buttonOutline: {
        width: '60%',
        height: 20,
        borderColor: '#92b5fa',
        borderStyle: 'solid',
        borderWidth: 1,
        backgroundColor: '#ffffff',
        alignItems: "center",
        borderRadius: 50,
        justifyContent: 'center',
        alignSelf: 'center',
        padding: 10,
        marginTop: 10,
        flexDirection:'row',
      },
      buttonText: {
        textAlign: 'center',
        fontSize: 14,
        color: "#92b5fa",
        fontFamily: 'NunitoSans-Bold',
      },
      emptyView: {
          justifyContent: 'center',
          alignSelf: 'center', 
          fontSize:17
      }
    


})
export default galleryView;