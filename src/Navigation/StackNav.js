import { createStackNavigator } from '@react-navigation/stack';
import React, { useEffect, useState, useContext } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { GalleryContext } from '../Context/Store';
import AsyncStorage from '@react-native-async-storage/async-storage';


import Home from '../Home';
import galleryView from '../galleryView';


const MainStack = createStackNavigator();



export default function StackNav() {
    const { media } = useContext(GalleryContext);
    const [uMedia, setUMedia] = media;
    const [appLoaded, setAppLoaded] = useState(false)

    useEffect(() => {
        async function getMedia() {
            const asyncMedia = await AsyncStorage.getItem('media');
             console.log('media in stacknav', asyncMedia)
            if (asyncMedia) {
                setUMedia(JSON.parse(asyncMedia));
                setAppLoaded(true);

                // console.log('media in stacknav', JSON.parse(media))

            } else {
                console.log('in else')
                setAppLoaded(true);
            }
        }

        getMedia();

       
    }, []
    )


if(appLoaded){
    return (
        <MainStack.Navigator initialRouteName={Home} screenOptions={{ headerTitleAlign: 'center' }} >
            <MainStack.Screen name='Home' component={Home} options={{ headerShown: false }} ></MainStack.Screen>
            <MainStack.Screen name='galleryView' component={galleryView} options={{ headerTitle: 'Gallery' }} ></MainStack.Screen>
        </MainStack.Navigator>
    )
}else{
    return null;
}
}
